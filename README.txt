README

This repository is a sample of code written by me (Natasha Bhatia) using public datasets. Most of the code I've written for research cannot be publicly shared because of NDAs, so this repository is a selection of streamlined code that uses only public data.

The code is meant to showcase my abilities in cleaning and analyzing data.

The data/ folder includes the data files.

The sample analyses/ folder is broken down into subfolders (for each different dataset) and contains codes in (primarily) R.


